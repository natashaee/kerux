use base64::{prelude::BASE64_URL_SAFE_NO_PAD, Engine};
use ring::digest::{digest, SHA256};
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value as JsonValue};

use crate::{
    events::{Event, EventContent},
    util::{domain::Domain, mxid::RoomId, MatrixId},
};

/// An unhashed (incomplete) Persistent Data Unit for room version 4.
/// This can only be used to construct a complete, hashed PDU.
#[derive(Serialize)]
pub struct UnhashedPdu {
    #[serde(flatten)]
    pub event_content: EventContent,
    pub room_id: RoomId,
    pub sender: MatrixId,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_key: Option<String>,
    #[serde(skip)]
    pub unsigned: Option<JsonValue>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub redacts: Option<String>,
    pub origin: Domain,
    pub origin_server_ts: i64,
    pub prev_events: Vec<String>,
    pub depth: i64,
    pub auth_events: Vec<String>,
}

/// A Persistent Data Unit (room event) for room version 4.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct PduV4 {
    #[serde(flatten)]
    pub event_content: EventContent,
    pub room_id: RoomId,
    pub sender: MatrixId,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_key: Option<String>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub unsigned: Option<JsonValue>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub redacts: Option<String>,
    pub origin: Domain,
    pub origin_server_ts: i64,
    pub prev_events: Vec<String>,
    pub depth: i64,
    pub auth_events: Vec<String>,
    pub hashes: EventHash,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub signatures: Option<Map<String, JsonValue>>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct EventHash {
    pub sha256: String,
}

impl UnhashedPdu {
    /// Turns self into a hashed RoomEventV4 by hashing its contents.
    ///
    /// Does not add any signatures.
    pub fn finalize(self) -> PduV4 {
        let json = cjson::to_string(&self).expect("event doesn't meet canonical json reqs");
        let content_hash = BASE64_URL_SAFE_NO_PAD.encode(digest(&SHA256, json.as_bytes()).as_ref());
        PduV4 {
            event_content: self.event_content,
            room_id: self.room_id,
            sender: self.sender,
            state_key: self.state_key,
            unsigned: self.unsigned,
            redacts: self.redacts,
            origin: self.origin,
            origin_server_ts: self.origin_server_ts,
            prev_events: self.prev_events,
            depth: self.depth,
            auth_events: self.auth_events,
            hashes: EventHash {
                sha256: content_hash,
            },
            signatures: Some(Map::new()),
        }
    }
}

impl PduV4 {
    /// Turns a PDU into a format which is suitable for clients.
    pub fn into_client_format(self) -> Event {
        let event_id = self.event_id();
        Event {
            event_content: self.event_content,
            room_id: Some(self.room_id),
            sender: self.sender,
            state_key: self.state_key,
            unsigned: self.unsigned,
            redacts: self.redacts,
            origin_server_ts: Some(self.origin_server_ts),
            event_id,
        }
    }

    pub fn redact(self) -> Self {
        PduV4 {
            event_content: self.event_content.redact(),
            room_id: self.room_id,
            sender: self.sender,
            state_key: self.state_key,
            unsigned: None,
            redacts: None,
            origin: self.origin,
            origin_server_ts: self.origin_server_ts,
            prev_events: self.prev_events,
            depth: self.depth,
            auth_events: self.auth_events,
            hashes: self.hashes,
            signatures: self.signatures,
        }
    }

    pub fn event_id(&self) -> String {
        let mut redacted = self.clone().redact();
        redacted.signatures = None;
        // age_ts doesn't exist, and unsigned already got blasted in redact()
        let json = cjson::to_string(&redacted).expect("event doesn't meet canonical json reqs");
        let mut event_id = BASE64_URL_SAFE_NO_PAD.encode(digest(&SHA256, json.as_bytes()).as_ref());
        event_id.insert(0, '$');
        event_id
    }
}
